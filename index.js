// TODO
const express = require('express')
const bodyParser = require("body-parser");
const app = express()
app.use(bodyParser.text());

const getNumbersAfterCommas = (num) => {
    let res = num.replace(".", ' ').split(" ");
    return res.length === 1
        ? 0
        : res[1].length
}
// ex1
app.post('/square', (req, res) => {

    let number = req.body;
    let pow = 2;
    let square = Math.pow(number, pow).toFixed(2 * getNumbersAfterCommas(number));

    res.send({
        "number": parseFloat(number),
        "square": parseFloat(square)
    });
})

//ex2
app.post("/reverse", (req, res) => {
    res.send(req.body.split("").reverse().join(""));
})

const getWeekDay = (year, month, day) => {
    let daysList = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let date = new Date(year, month, day);
    return daysList[date.getDay()];
}

const getDaysDifference = (year, month, day) => {

    let defaultHours = 0;
    let seconds = 60;
    let minutes = 60;
    let hours = 24;

    let defaultDay = new Date(year, month, day, defaultHours, 0, 0);
    let currentDay = new Date();
    currentDay.setHours(defaultHours);
    currentDay.setMinutes(defaultHours);
    currentDay.setSeconds(defaultHours);

    return Math.round(Math.abs(currentDay- defaultDay) / (1000 * seconds * minutes * hours));
}

//ex3
app.get("/date/:year/:month/:day", (req, res) => {

    let year = req.params.year;
    let month = req.params.month - 1;
    let day = req.params.day;

    res.send({
        "weekDay": getWeekDay(year, month, day),
        "isLeapYear": ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0),
        "difference": getDaysDifference(year, month, day)
    })

})

app.listen(process.env.PORT || 56201, () => {
    console.log(`Lol`)
})